var gulp = require("gulp"),
    browserSync = require("browser-sync"),
    karma = require("karma").server,
    server = require("gulp-live-server"),
    protractor = require("gulp-protractor").protractor;
    
gulp.task("test-browser", function(done){
    karma.start({
        configFile: __dirname + "/karma.conf.js",
        singleRun: true,
        reporters: ["mocha", "coverage"]
    },function(){
        done();
    });
});

gulp.task("serve-coverage", ["test-browser"], function(){
    browserSync.init({
        notify: false,
        port: 7777,
        server:{
            baseDir: ["test/coverage", "app"]
        }
    });
    
    gulp.watch(["app/**/*.*"])
        .on("change", browserSync.reload);
});

gulp.task("serve-test", function(){
    browserSync.init({
        notify: false,
        port: 8081,
        server:{
            baseDir: ["test", "app"],
            routes:{
                "/bower_components": "bower_components"
            }
        }
    });
    
    gulp.watch(["app/**/*.*"])
        .on("change", browserSync.reload);
});
    
gulp.task("server", function(){
    var live = new server("server.js");
    live.start();
});

gulp.task("serve", ["server"],function(){
    browserSync.init({
        notify: false,
        port: 8080,
        server:{
            baseDir: ["app"],
            routes:{
                "/bower_components": "bower_components"
            }
        }
    });
    
    gulp.watch(["app/**/*.*"])
        .on("change", browserSync.reload);
});

gulp.task("protractor", ["serve"], function(done){
    gulp.src(["test/e2e/*.js"])
        .pipe(protractor({
            configFile: "test/protractor-config.js",
            args: ["--baseUrl", "http://localhost:8080"]
        }))
        .on("end", done);
});

