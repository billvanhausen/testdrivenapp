angular.module("AddressBook", [])

.service("contactService", ["$http", "$log", function($http, $log){
    var contactService = this;
    contactService.contacts = [];
    
    $http.get("http://localhost:9001/contacts")
    .then(function(res){
        $log.log(res);
        while(res.data[0]){
            contactService.contacts.push(res.data.pop());            
        }
    });
    
    this.addContact = function(contact){
        contactService.contacts.push(contact);
    };
}])

.controller("ContactController", ["$scope", "contactService", function($scope, contactService){
    $scope.contacts = contactService.contacts;
    
}])

.controller("AddContact", ["$scope", "contactService", function($scope, contactService){
    $scope.addContact = function(){
        contactService.addContact($scope.contact);
    };
}])

.filter("proper", function(){
    return function(name){
        var type = typeof name;
        
        if(type !== "number" && type !== "string") throw new Error();
        
        return name.toString().split(" ").map(function(word){
            return word[0].toUpperCase().concat(word.slice(1));
        }).join(" ");
    }
})

.directive("avatar", function(){
    return {
        restrict: "AE",
        scope: {
            name:"=",
        },
        template:"<span class='avatar'>{{ name[0] | proper }}</span>"
    }
});

