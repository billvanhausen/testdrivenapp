var express = require("express"),
    cors = require("cors"),
    app = express();
    
app.use(cors());

var contacts = [
    {
        name:"eddard style",
        age: 26,
        occupation: "Concept Artist",
        email: "estyle@games.com"
    },
    {
        name:"robert fray",
        age: 30,
        occupation: "Musician",
        email: "robert@jamsession.com"
    }
];

app.get("/contacts", function(req, res){
    res.status(200).json(contacts);
});

app.listen(9001);